import pickle
import numpy as np

directory_name = "./results/"
files = ["batch_sizes.pkl",
         "hidden_layer_size.pkl",
         "initial_ranges.pkl",
         "learning_rates.pkl",
         "momentums.pkl",
         "hidden_layer_size_relu.pkl"]

variable_n = ["batch_size", "layer_size", "initial_range", "learning_rate", "momentum", "layer_size"]


def load_data(file_name):
    with open(file_name, 'rb') as f:
        return pickle.load(f, encoding='bytes')


def to_graph(results, variable):
    colors = ["blue", "green", "red", "brown", "black", "yellow", "purple", "orange", "teal", "lime", "olive"]
    legend = ""
    plots = ""
    var = "loss_arr"
    v = variable.replace('_', r'\_')
    for i in range(len(results)):
        if variable == "batch_size":
            results[i]["batch_size"] = [50, 100, 150, 200, 250][i]
        values = ""
        for j in range(len(results[i][var])):
            values += f"({j},{results[i][var][j]})"
        plot = r'\addplot[' + '\n' + \
               f'    color={colors[i]},' + '\n' + \
               r'    ]' + '\n' + \
               r'    coordinates {' + '\n' + \
               f'    {values}' + '\n' + \
               r'    };' + '\n' + \
               r' ' + '\n'
        plots += plot
        legend += f'{results[i][variable]}, '

    results1 = r'\begin{figure}[H]' + '\n' + \
               r'\centering' + '\n' + \
               r'\begin{tikzpicture}' + '\n' + \
               r'\begin{axis}[' + '\n' + \
               r'width=\textwidth,' + '\n' + \
               r'height=0.6\textwidth,' + '\n' + \
               r'    xlabel={Epoka},' + '\n' + \
               r'    ylabel={Wartość funkcji kosztu},' + '\n' + \
               r'    legend pos=north east,' + '\n' + \
               r'    ymajorgrids=true,' + '\n' + \
               r'    grid style=dashed,' + '\n' + \
               r']' + '\n' + \
               r' ' + '\n' + \
               f'{plots}' + '\n' + \
               r'    \legend{' + legend + r'}' + '\n' + \
               r' ' + '\n' + \
               r'\end{axis}' + '\n' + \
               r'\end{tikzpicture}' + '\n' + \
               r'\caption{Zmiana wartości funkcji kosztu w zależności od ' + f'{v}' + '}\n' + \
               r'\end{figure}'

    legend = ""
    plots = ""
    var = "accuracy_arr"
    for i in range(len(results)):
        values = ""
        for j in range(len(results[i][var])):
            values += f"({j},{results[i][var][j]})"
        plot = r'\addplot[' + '\n' + \
               f'    color={colors[i]},' + '\n' + \
               r'    ]' + '\n' + \
               r'    coordinates {' + '\n' + \
               f'    {values}' + '\n' + \
               r'    };' + '\n' + \
               r' ' + '\n'
        plots += plot
        v = variable.replace('_', r'\_')
        legend += f'{results[i][variable]}, '

    results2 = r'\begin{figure}[H]' + '\n' + \
               r'\centering' + '\n' + \
               r'\begin{tikzpicture}' + '\n' + \
               r'\begin{axis}[' + '\n' + \
               r'width=\textwidth,' + '\n' + \
               r'height=0.6\textwidth,' + '\n' + \
               r'    xlabel={Epoka},' + '\n' + \
               r'    ylabel={Wartość funkcji kosztu},' + '\n' + \
               r'    legend pos=south east,' + '\n' + \
               r'    ymajorgrids=true,' + '\n' + \
               r'    grid style=dashed,' + '\n' + \
               r']' + '\n' + \
               r' ' + '\n' + \
               f'{plots}' + '\n' + \
               r'    \legend{' + legend + r'}' + '\n' + \
               r' ' + '\n' + \
               r'\end{axis}' + '\n' + \
               r'\end{tikzpicture}' + '\n' + \
               r'\caption{Zmiana skuteczności rozpoznawania cyfr w zależności od ' + f'{v}' + '}\n' + \
               r'\end{figure}'
    return results1, results2


def to_table(result, variable):
    data = ""
    for r in result:
        if r['time_pass'] / 60 > 30:
            data += r'		\rowcolor{Gray}' + '\n'
        data += f'		{r[variable]} & ' + '{:.2f}'.format(
            r["final_accuracy"] * 100) + f' & ' + '{:.2f}'.format(
            r["time_pass"] / 60) + f' & {r["epochs"]} \\\\ \\hline' + '\n'
    result = r'\begin{table}[H]' + '\n' + \
             r'	\caption{Trafność predykcji wyuczonego modelu na zbiorze testowym}' + '\n' + \
             r'	\centering' + '\n' + \
             r'	\begin{tabular}{|c|c|c|c|}' + '\n' + \
             r'		\hline' + '\n' + \
             f'		{variable} & Trafność [\\%]& Czas uczenia [min] & Liczba epok \\\\ \\hline' + '\n' + \
             f'     {data}' + '\n' + \
             r'	\end{tabular}' + '\n' + \
             r'\end{table}'
    return result


def to_description(result):
    layer_size = "layer_size"
    initial_range = "initial_range"
    learning_rate = "learning_rate"
    momentum = "momentum"
    return f"Współczynnik uczenia: {result[0][learning_rate]} \\\\ \n" + \
           f"Przedział losowania początkowych wartości wag: {result[0][initial_range]} \\\\ \n" + \
           f"Wartość momentum: {result[0][momentum]} \\\\ \n" + \
           f"Rozmiar warstwy ukrytej: {result[0][layer_size]} \\\\ \n" + \
           f"Rozmiar mini-batcha: 100 \\\\ \n"


for i in range(len(files)):
    data = load_data(directory_name + files[i])
    loss, accuracy = to_graph(data, variable_n[i])
    description = to_description(data)
    table = to_table(data, variable_n[i])
    with open(f'./report/charts/{files[i]}graph.tex', 'w+') as f:
        f.write(description)
        f.write("\n\n\n")
        f.write(table)
        f.write("\n\n\n")
        f.write(loss)
        f.write("\n\n\n")
        f.write(accuracy)
