import numpy as np
import pickle
import random
import optim


def stable_softmax(x):
    exps = np.exp(x - np.max(x))
    return exps / np.sum(exps)


activation_functions = {
    "sigmoid": ((lambda x: 1 / (1 + np.exp(-x))),
                (lambda x: np.exp(-x) / (1 + np.exp(-x)) / (1 + np.exp(-x)))),
    "softmax": ((lambda x: stable_softmax(x)),
                (lambda x: (np.exp(x) / np.sum(np.exp(x))) - (np.exp(x) / np.sum(np.exp(x))) ** 2)),
    "relu": ((lambda x: (np.maximum(0, x))),
             (lambda x: (np.heaviside(x, 1))))
}


def uniform_random(dim, rng):
    return np.fromfunction(lambda i, j: np.vectorize(lambda ii, jj: random.uniform(rng[0], rng[1]))(i, j), dim,
                           dtype=float)


def load_data(file_name):
    with open(file_name, 'rb') as data:
        return pickle.load(data, encoding='bytes')


def save_model(model, file_name):
    obj = []
    for layer in model.sequence:
        obj.append({
            "input_dim": layer.input_dim,
            "output_dim": layer.output_dim,
            "weights": layer.weights,
            "bias": layer.bias,
            "activation": layer.activation_type
        })
    with open(file_name, 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)


def load_model(file_name):
    model = Model()
    model.sequence = []
    with open(file_name, 'rb') as f:
        data = pickle.load(f, encoding='bytes')
        for l in data:
            layer = NNLayer(l["input_dim"], l["output_dim"], l["activation"])
            layer.weights = l["weights"]
            layer.bias = l["bias"]
            model.sequence.append(layer)
    return model


class NNLayer(object):
    def __init__(self, input_dim, output_dim, activation, init_range=(-1, 1)):
        self.input_dim = input_dim
        self.output_dim = output_dim
        self.activation_type = activation
        self.inputs = np.zeros(input_dim)
        self.z = np.zeros(output_dim)
        self.outputs = np.zeros(output_dim)
        self.activation = activation_functions[activation]
        self.weights = uniform_random((output_dim, input_dim), init_range)
        self.bias = uniform_random((output_dim, 1), init_range)

    def forward(self, x):
        self.inputs = x.T
        self.z = self.weights @ self.inputs + self.bias
        self.outputs = np.apply_along_axis(
            lambda x: self.activation[0](x), axis=0, arr=self.z)
        return self.outputs.T


class Model(object):
    def __init__(self, layers=[]):
        self.sequence = layers

    def forward(self, x):
        for layer in self.sequence:
            x = layer.forward(x)
        return np.array(x)

    def parameters(self):
        return self.sequence

    def backpropagation(self, predicted, desired_output):
        d_weight = []
        d_bias = []
        error = np.subtract(desired_output, predicted).T
        for r_layer in reversed(self.sequence):
            delta = error * \
                    np.apply_along_axis(
                        lambda x: r_layer.activation[1](x), axis=1, arr=r_layer.z)  # or arr=r_layer.output
            d_weight.append((delta @ r_layer.inputs.T) / len(predicted))
            d_bias.append(np.mean(a=delta, axis=1, keepdims=True))
            error = r_layer.weights.T @ delta
        return zip(d_weight, d_bias)


def one_hot_encoder(labels):
    ohe = []
    for l in labels:
        ohe.append([1 if i == l else 0 for i in range(10)])
    return np.array(ohe)


def test_accuracy(model, data, labels):
    result = model.forward(data)
    return np.sum(labels == np.argmax(result, axis=1)) / len(labels)


def mean_squared_error(actual, predicted):
    error = np.subtract(actual, predicted)
    sums_square_error = np.sum(error ** 2)
    mean_square_errors = sums_square_error / len(actual)
    return mean_square_errors

#
# train, val, test = load_data("./resources/mnist.pkl")
# one_hot_encoded = one_hot_encoder(train[1])
#
# net = Model([NNLayer(784, 400, "relu"),
#              # NNLayer(400, 100, "sigmoid"),
#              NNLayer(400, 10, "softmax")])
#
# optimizer = optim.SGD(net.parameters(), learning_rate=0.1, momentum=0.9)
#
# batch_size = 50
# val_one_hot_encoded = one_hot_encoder(val[1])
# for i in range(10):
#     print(i)
#     for j in range(0, len(train[1]), batch_size):
#         result = net.forward(np.array(train[0][j:j + batch_size]))
#         loss = net.backpropagation(result, np.array(one_hot_encoded[j:j + batch_size]))
#         optimizer.step(loss)
#     print(mean_squared_error(val_one_hot_encoded, net.forward(np.array(val[0]))))
#     print(f"accuracy: {test_accuracy(net, val[0], val[1])}")
#
# print(f"accuracy: {test_accuracy(net, test[0], test[1])}")
#
# save_model(net, "./model.pkl")
# model = load_model("./model.pkl")
# print("loadedmodel")
# test_accuracy(model, test[0], test[1])
