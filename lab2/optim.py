import numpy as np


class SGD(object):
    def __init__(self, parameters, learning_rate, momentum=0.9):
        self.learning_rate = learning_rate
        self.momentum = momentum
        self.parameters = parameters
        self.prev_loss = [[np.zeros_like(l.weights), np.zeros_like(l.bias)] for l in reversed(parameters)]

    def step(self, loss):
        for d in zip(reversed(self.parameters), loss, self.prev_loss):
            d[2][0] = self.momentum * d[2][0] + self.learning_rate * d[1][0]
            d[0].weights += d[2][0]
            d[2][1] = self.momentum * d[2][1] + self.learning_rate * d[1][1]
            d[0].bias += d[2][1]
