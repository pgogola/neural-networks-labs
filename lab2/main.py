from nn_layer import (
    Model,
    NNLayer,
    save_model,
    load_model,
    load_data,
    one_hot_encoder,
    mean_squared_error,
    test_accuracy,
)
import optim
import numpy as np
import time
import pickle

# hidden_layer_sizes = [(400, 100),
#                       (400, 200),
#                       (300, 100),
#                       (500, 300)]
# hidden_layer_sizes = [100,
#                       200,
#                       300,
#                       400,
#                       500,
#                       600]
#
# batch_sizes = [50, 100, 150, 200, 250]
# learning_rates = [0.5, 0.1, 0.05, 0.01, 0.005, 0.001]
# initial_ranges = [(-1.0, 1.0),
#                   (-0.8, 0.8),
#                   (-0.6, 0.6),
#                   (-0.4, 0.4),
#                   (-0.2, 0.2)]
# momentums = [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]

hidden_layer_sizes = [100,
                      200,
                      300,
                      400,
                      500,
                      600]

batch_sizes = [50, 100, 150, 200, 250]
learning_rates = [0.5, 0.1, 0.05, 0.01, 0.005, 0.001]
initial_ranges = [(-1.0, 1.0),
                  (-0.8, 0.8),
                  (-0.6, 0.6),
                  (-0.5, 0.5),
                  (-0.4, 0.4),
                  (-0.2, 0.2)]
momentums = [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]


def test_model(layer_sizes, batch_sizes, learning_rates, initial_ranges, momentums, repeats):
    test_results = []
    loss_margin = 0.05
    max_time_available = 30 * 60  # 30 minutes
    for _ in range(repeats):
        for layer_size in layer_sizes:
            for initial_range in initial_ranges:
                for learning_rate in learning_rates:
                    for momentum in momentums:
                        for batch_size in batch_sizes:
                            net = Model([NNLayer(784, layer_size, "relu", initial_range),
                                         # NNLayer(layer_size[0], layer_size[1], "sigmoid", initial_range),
                                         NNLayer(layer_size, 10, "softmax", initial_range)])
                            optimizer = optim.SGD(net.parameters(), learning_rate=learning_rate, momentum=momentum)
                            min_loss = float("inf")
                            loss_arr = []
                            accuracy_arr = []
                            stop_condition = "overfitting"
                            start_time = time.time()
                            time_pass = 0
                            epochs = 0
                            while True:
                                epochs += 1
                                for j in range(0, len(train[1]), batch_size):
                                    result = net.forward(np.array(train[0][j:j + batch_size]))
                                    loss = net.backpropagation(result, np.array(
                                        one_hot_encoded[j:j + batch_size]))
                                    optimizer.step(loss)
                                current_loss = mean_squared_error(val_one_hot_encoded,
                                                                  net.forward(np.array(val[0])))
                                print(current_loss)
                                if current_loss < min_loss + loss_margin:
                                    if current_loss < min_loss:
                                        min_loss = current_loss
                                        save_model(net, "./model.pkl")
                                else:
                                    break
                                current_accuracy = test_accuracy(net, val[0], val[1])
                                loss_arr.append(current_loss)
                                accuracy_arr.append(current_accuracy)
                                time_pass = time.time() - start_time
                                if time_pass > max_time_available:
                                    stop_condition = "time"
                                    break

                            net = load_model("./model.pkl")
                            final_accuracy = test_accuracy(net, test[0], test[1])
                            test_results.append({
                                "layer_size": layer_size,
                                "initial_range": initial_range,
                                "learning_rate": learning_rate,
                                "momentum": momentum,
                                "loss_arr": loss_arr,
                                "accuracy_arr": accuracy_arr,
                                "final_accuracy": final_accuracy,
                                "stop_condition": stop_condition,
                                "time_pass": time_pass,
                                "epochs": epochs
                            })
    return test_results


if __name__ == "__main__":
    train, val, test = load_data("./resources/mnist.pkl")
    one_hot_encoded = one_hot_encoder(train[1])
    val_one_hot_encoded = one_hot_encoder(val[1])

    print("hidden_layer_size")
    results = test_model(layer_sizes=hidden_layer_sizes, batch_sizes=[batch_sizes[1]],
                         learning_rates=[learning_rates[3]],
                         initial_ranges=[initial_ranges[3]], momentums=[momentums[6]], repeats=1)
    file_name = "hidden_layer_size_relu.pkl"
    with open(file_name, 'wb') as f:
        pickle.dump(results, f, pickle.HIGHEST_PROTOCOL)

    # print("batch_sizes")
    # results = test_model(layer_sizes=[hidden_layer_sizes[3]], batch_sizes=batch_sizes,
    #                      learning_rates=[learning_rates[3]],
    #                      initial_ranges=[initial_ranges[3]], momentums=[momentums[6]], repeats=1)
    # file_name = "batch_sizes.pkl"
    # with open(file_name, 'wb') as f:
    #     pickle.dump(results, f, pickle.HIGHEST_PROTOCOL)
    #
    # print("learning_rates")
    # results = test_model(layer_sizes=[hidden_layer_sizes[3]], batch_sizes=[batch_sizes[1]],
    #                      learning_rates=learning_rates,
    #                      initial_ranges=[initial_ranges[3]], momentums=[momentums[6]], repeats=1)
    # file_name = "learning_rates.pkl"
    # with open(file_name, 'wb') as f:
    #     pickle.dump(results, f, pickle.HIGHEST_PROTOCOL)
    #
    # print("initial_ranges")
    # results = test_model(layer_sizes=[hidden_layer_sizes[3]], batch_sizes=[batch_sizes[1]],
    #                      learning_rates=[learning_rates[3]],
    #                      initial_ranges=initial_ranges, momentums=[momentums[6]], repeats=1)
    # file_name = "initial_ranges.pkl"
    # with open(file_name, 'wb') as f:
    #     pickle.dump(results, f, pickle.HIGHEST_PROTOCOL)
    #
    # print("momentums")
    # results = test_model(layer_sizes=[hidden_layer_sizes[3]], batch_sizes=[batch_sizes[1]],
    #                      learning_rates=[learning_rates[3]],
    #                      initial_ranges=[initial_ranges[3]], momentums=momentums, repeats=1)
    # file_name = "momentums.pkl"
    # with open(file_name, 'wb') as f:
    #     pickle.dump(results, f, pickle.HIGHEST_PROTOCOL)
