import numpy as np

from adaline import (
    Adaline,
    bipolar_activation,
    and_tab_bipolar,
    or_tab_bipolar
)

from perceptron import (
    Perceptron,
    binar_activation,
    and_tab_binar,
    or_tab_binar
)


def weight_tests(model_type, activation_function, train_data, repeats):
    ranges = [
        (-1.0, 1.0),
        (-0.9, 0.9),
        (-0.8, 0.8),
        (-0.7, 0.7),
        (-0.6, 0.6),
        (-0.5, 0.5),
        (-0.4, 0.4),
        (-0.3, 0.3),
        (-0.2, 0.2),
        (-0.1, 0.1)
    ]
    lr = 0.001
    result = []
    for r in ranges:
        for _ in range(repeats):
            model = model_type(2, activation_function, r)
            iterations = model.train(train_data, lr)
            result.append((r, iterations))
            print(
                f"Training for weight in range {r} took {iterations} iterations")
    return result


def learning_rate_tests(model_type, activation_function, train_data, repeats):
    rng = (-1.0, 1.0)
    lrs = [0.5, 0.1, 0.05, 0.01, 0.005, 0.001]
    result = []
    for lr in lrs:
        for _ in range(repeats):
            model = model_type(2, activation_function, rng)
            iterations = model.train(train_data, lr)
            result.append((lr, iterations))
            print(
                f"Training for learning rate {lr} took {iterations} iterations")
    return result


def activation_function_tests(model_type, activation_function, train_data, repeats):
    rng = (-1.0, 1.0)
    lr = 0.01
    result = []
    print(train_data)
    for _ in range(repeats):
        model = model_type(2, activation_function, rng)
        iterations = model.train(train_data, lr)
        result.append(
            (activation_function.__name__.replace('_', ' '), iterations))
        print(
            f"Training for activation function {activation_function.__name__.replace('_', ' ')} took {iterations} iterations")
    return result


weight_test_result_and = weight_tests(
    Perceptron, binar_activation, and_tab_binar, 10)
weight_test_result_or = weight_tests(
    Perceptron, binar_activation, or_tab_binar, 10)
learning_rate_test_result_and = learning_rate_tests(
    Perceptron, binar_activation, and_tab_binar, 10)
learning_rate_test_result_or = learning_rate_tests(
    Perceptron, binar_activation, or_tab_binar, 10)
binar_test_resutl_and = activation_function_tests(
    Perceptron, binar_activation, and_tab_binar, 10)
binar_test_resutl_or = activation_function_tests(
    Perceptron, binar_activation, or_tab_binar, 10)
bipolar_test_resutl_and = activation_function_tests(
    Perceptron, bipolar_activation, and_tab_bipolar, 10)
bipolar_test_resutl_or = activation_function_tests(
    Perceptron, bipolar_activation, or_tab_bipolar, 10)


def to_table(results):
    result = r'\begin{table}[H]' + '\n' + \
        r'\caption{Uśredniona liczba epok wymagana do wyuczenia modelu dla przypadku funkcji OR}' + '\n' + \
        r'\label{tab: perc_lr_or}' + '\n' + \
        r'\centering' + '\n' + \
        r'\begin{tabular}{|c|c|}' + '\n' + \
        r'	\hline' + '\n' + \
        r'	Przedział losowania wag & Średnia liczba epok \\ \hline' + '\n' + \
        r'\end{tabular}' + '\n' + \
        r'\end{table}' + '\n'
    weight_test_result_and_mean = {}
    for rng, iters in results:
        if not rng in weight_test_result_and_mean:
            weight_test_result_and_mean[rng] = []
        weight_test_result_and_mean[rng].append(iters)

    for i in weight_test_result_and_mean.keys():
        weight_test_result_and_mean[i] = np.mean(
            weight_test_result_and_mean[i])
    values = ''
    for i in weight_test_result_and_mean.keys():
        # print(f"{i} & {weight_test_result_and_mean[i]}\\\\ \\hline")
        values += f"{i} & {weight_test_result_and_mean[i]}\\\\ \\hline\n"
    result = r'\begin{table}[H]' + '\n' + \
        r'\caption{Uśredniona liczba epok wymagana do wyuczenia modelu dla przypadku funkcji OR}' + '\n' + \
        r'\label{tab: perc_lr_or}' + '\n' + \
        r'\centering' + '\n' + \
        r'\begin{tabular}{|c|c|}' + '\n' + \
        r'	\hline' + '\n' + \
        r'	Przedział losowania wag & Średnia liczba epok \\ \hline' + '\n' + \
        values + '\n' + \
        r'\end{tabular}' + '\n' + \
        r'\end{table}' + '\n'
    print(result)


def to_graph(results):
    coordinates = 'coordinates {'
    xticklabels = 'xticklabels={'
    xtick = 'xtick = {'
    weight_test_result_and_mean = {}
    for rng, iters in results:
        if not rng in weight_test_result_and_mean:
            weight_test_result_and_mean[rng] = []
        weight_test_result_and_mean[rng].append(iters)

    for i in weight_test_result_and_mean.keys():
        weight_test_result_and_mean[i] = np.mean(
            weight_test_result_and_mean[i])

    num = 1
    for i in weight_test_result_and_mean.keys():
        coordinates += f'({num}, {weight_test_result_and_mean[i]})'
        xticklabels += (f'{i}' + (', ' if num !=
                                  len(weight_test_result_and_mean) else ''))
        xtick += (f'{num}' + (', ' if num !=
                              len(weight_test_result_and_mean) else ''))
        num += 1

    coordinates += '};'
    xticklabels += '},'
    xtick += '},'
    result = r'\begin{figure}[H]' + '\n' + \
        r'\centering' + '\n' + \
        r'\begin{tikzpicture}' + '\n' + \
        r'\begin{axis}[' + '\n' + \
        r'width=\textwidth,' + '\n' + \
        r'height=0.6\textwidth,' + '\n' + \
        r'x tick label style={font=\small},' + '\n' + \
        xtick + '\n' + \
        xticklabels + '\n' + \
        r'ylabel=Liczba epok,' + '\n' + \
        r'xlabel=Przedzia,' + '\n' + \
        r'enlargelimits=0.05,' + '\n' + \
        r'legend style={at={(0.5, -0.1)},' + '\n' + \
        r'              anchor=north, legend columns=-1},' + '\n' + \
        r'ybar,' + '\n' + \
        r']' + '\n' + \
        r'\addplot' + '\n' + \
        coordinates + '\n' + \
        r'\end{axis}' + '\n' + \
        r'\end{tikzpicture}' + '\n' + \
        r'\caption{caption}' + '\n' + \
        r'\end{figure}'
    print(result)


print("AND")
to_table(weight_test_result_and)
to_graph(weight_test_result_and)
print("OR")
to_table(weight_test_result_or)
to_graph(weight_test_result_or)
print()
print()

print("AND")
to_table(learning_rate_test_result_and)
to_graph(learning_rate_test_result_and)
print("OR")
to_table(learning_rate_test_result_or)
to_graph(learning_rate_test_result_or)
print()
print()

print("AND")
to_table(binar_test_resutl_and)
to_graph(binar_test_resutl_and)
to_table(bipolar_test_resutl_and)
to_graph(bipolar_test_resutl_and)
print("OR")
to_table(binar_test_resutl_or)
to_graph(binar_test_resutl_or)
to_table(bipolar_test_resutl_or)
to_graph(bipolar_test_resutl_or)
print()
print()
