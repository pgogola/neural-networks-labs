import numpy as np
import random


def bipolar_activation(x):
    return 1 if x > 0 else -1


class Adaline(object):
    def __init__(self, input_dim, activation_fun, init_w_range=(-1.0, 1.0)):
        self.input_dim = input_dim
        self.activation_fun = activation_fun
        self.weights = np.array([random.uniform(init_w_range[0], init_w_range[1])
                                 for _ in range(input_dim+1)])
        self.weights[input_dim] = random.uniform(
            init_w_range[0], init_w_range[1])

    def predict(self, x):
        z = np.dot(self.weights[:self.input_dim],
                   x)+self.weights[self.input_dim]
        return self.activation_fun(z), z

    def train(self, train_data, learning_rate, error):
        MSE = float('inf')
        prev_MSE = None
        epochs = 0
        while MSE > error and prev_MSE != MSE:
            errors = []
            for x, label in train_data:
                predicted, z = self.predict(x)
                errors.append(label-z)
                delta = label-z
                self.weights[:self.input_dim] += learning_rate*delta*x
                self.weights[self.input_dim] += learning_rate*delta
            prev_MSE = MSE
            MSE = np.mean(np.power(errors, 2))
            epochs += 1
        return epochs, prev_MSE != MSE


and_tab_bipolar = np.array([(np.array([-1, -1]), -1),
                            (np.array([-1, 1]), -1),
                            (np.array([1, -1]), -1),
                            (np.array([1, 1]), 1)])

or_tab_bipolar = np.array([(np.array([-1, -1]), -1),
                           (np.array([-1, 1]), 1),
                           (np.array([1, -1]), 1),
                           (np.array([1, 1]), 1)])

# # ADALINE AND FUN
# perceptron = Adaline(2, bipolar_activation)
# epochs, below_error = perceptron.train(and_tab_bipolar, 0.01, 0.3)
# print(
#     f'UCZENIE:\nLiczba epok: {epochs}, błąd poniżej zadanej wartości: {below_error}')
# for x, _ in and_tab_bipolar:
#     print(perceptron.predict(x)[0])

# # # ADALINE OR FUN
# perceptron = Adaline(2, bipolar_activation)
# epochs, below_error = perceptron.train(or_tab_bipolar, 0.01, 0.3)
# print(
#     f'UCZENIE:\nLiczba epok: {epochs}, błąd poniżej zadanej wartości: {below_error}')
# for x, _ in or_tab_bipolar:
#     print(perceptron.predict(x)[0])


# print(np.mean(np.power([2, 2], 2)))
