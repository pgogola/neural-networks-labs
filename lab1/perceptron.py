import numpy as np
import random


def binar_activation(x):
    return 1 if x > 0 else 0


def bipolarar_activation(x):
    return 1 if x > 0 else -1


class Perceptron(object):
    def __init__(self, input_dim, activation_fun, init_w_range=(-1.0, 1.0)):
        self.input_dim = input_dim
        self.activation_fun = activation_fun
        self.weights = np.array([random.uniform(init_w_range[0], init_w_range[1])
                                 for _ in range(input_dim+1)])

    def predict(self, x):
        return self.activation_fun(np.dot(self.weights[:self.input_dim], x)+self.weights[self.input_dim])

    def train(self, train_data, learning_rate, epochs=None):
        prev_weighs = None
        epochs = 0
        while not np.array_equal(prev_weighs, self.weights):
            prev_weighs = np.copy(self.weights)
            for x, label in train_data:
                predicted = self.predict(x)
                delta = label-predicted
                self.weights[:self.input_dim] += learning_rate*delta*x
                self.weights[self.input_dim] += learning_rate*delta
            epochs += 1
        return epochs


and_tab_binar = np.array([(np.array([0, 0]), 0),
                          (np.array([0, 1]), 0),
                          (np.array([1, 0]), 0),
                          (np.array([1, 1]), 1)])

or_tab_binar = np.array([(np.array([0, 0]), 0),
                         (np.array([0, 1]), 1),
                         (np.array([1, 0]), 1),
                         (np.array([1, 1]), 1)])


# # PERCEPTRON AND FUN
# perceptron = Perceptron(2, binar_activation)
# perceptron.train(and_tab_binar, 0.01)
# for x, _ in and_tab_binar:
#     print(perceptron.predict(x))

# # PERCEPTRON OR FUN
# perceptron = Perceptron(2, binar_activation)
# perceptron.train(or_tab_binar, 0.01)
# for x, _ in or_tab_binar:
#     print(perceptron.predict(x))

# print(perceptron.predict([-0.9, 1.1]))

# and_tab_bipolar = np.array([(np.array([-1, -1]), -1),
#                             (np.array([-1, 1]), -1),
#                             (np.array([1, -1]), -1),
#                             (np.array([1, 1]), 1)])

# or_tab_bipolar = np.array([(np.array([-1, -1]), -1),
#                            (np.array([-1, 1]), 1),
#                            (np.array([1, -1]), 1),
#                            (np.array([1, 1]), 1)])

# # PERCEPTRON AND FUN
# perceptron = Perceptron(2, bipolarar_activation)
# perceptron.train(and_tab_bipolar, 0.01)
# for x, _ in and_tab_bipolar:
#     print(perceptron.predict(x))

# # PERCEPTRON OR FUN
# perceptron = Perceptron(2, bipolarar_activation)
# perceptron.train(or_tab_bipolar, 0.01)
# for x, _ in or_tab_bipolar:
#     print(perceptron.predict(x))
