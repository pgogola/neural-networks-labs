import numpy as np


# np.seterr(all='raise')


class MSELoss(object):
    def __init__(self):
        self.error = 0
        self.d_error = 0

    def __call__(self, predicted, desired_output):
        self.error = np.mean(np.subtract(desired_output, predicted) ** 2)
        self.d_error = np.subtract(desired_output, predicted).T
        return self.error, self.d_error


class AbsoluteErrorLoss(object):
    def __init__(self):
        self.error = 0
        self.d_error = 0

    def __call__(self, predicted, desired_output):
        epsilon = 1e-8
        error = np.mean(np.abs(np.subtract(desired_output, predicted)))
        d_error = (np.subtract(desired_output, predicted) / (
                np.abs(np.subtract(desired_output, predicted)) + epsilon)).T
        return error, d_error


class Optimizer(object):
    def __init__(self, parameters):
        self.parameters = parameters

    def forward(self, error):
        d_weight = []
        d_bias = []
        for r_layer in reversed(self.parameters):
            delta = error * \
                    np.apply_along_axis(
                        lambda x: r_layer.activation[1](x), axis=1, arr=r_layer.z)  # or arr=r_layer.output
            d_weight.append((delta @ r_layer.inputs.T) / delta.shape[1])
            d_bias.append(np.mean(a=delta, axis=1, keepdims=True))
            error = r_layer.weights.T @ delta
        return zip(d_weight, d_bias)


class SGD(Optimizer):
    def __init__(self, parameters, learning_rate, momentum=0.9, **kwargs):
        super().__init__(parameters)
        self.learning_rate = learning_rate
        self.momentum = momentum
        self.prev_loss = [[np.zeros_like(l.weights), np.zeros_like(l.bias)] for l in reversed(parameters)]

    def step(self, loss):
        for d in zip(reversed(self.parameters), super().forward(loss), self.prev_loss):
            d[2][0] = self.momentum * d[2][0] + self.learning_rate * d[1][0]
            d[0].weights += d[2][0]
            d[2][1] = self.momentum * d[2][1] + self.learning_rate * d[1][1]
            d[0].bias += d[2][1]


class AdaGrad(Optimizer):
    def __init__(self, parameters, learning_rate=0.01, **kwargs):
        super().__init__(parameters)
        self.epsilon = 1e-8
        self.learning_rate = learning_rate
        self.parameters = parameters
        self.G = [[np.zeros_like(l.weights), np.zeros_like(l.bias)] for l in
                  reversed(parameters)]

    def step(self, loss):
        for d in zip(reversed(self.parameters), super().forward(loss), self.G):
            d[2][0] += d[1][0] ** 2
            eta_w = self.learning_rate / np.sqrt(d[2][0] + self.epsilon)
            d[0].weights += eta_w * d[1][0]
            d[2][1] += d[1][1] ** 2
            eta_b = self.learning_rate / np.sqrt(d[2][1] + self.epsilon)
            d[0].bias += eta_b * d[1][1]


class AdaDelta(Optimizer):
    def __init__(self, parameters, **kwargs):
        super().__init__(parameters)
        self.epsilon = 1e-4
        self.gamma = 0.0
        self.parameters = parameters
        self.G = [[np.zeros_like(l.weights), np.zeros_like(l.bias)] for l in reversed(parameters)]
        self.Theta = [[np.zeros_like(l.weights), np.zeros_like(l.bias)] for l in reversed(parameters)]

    def step(self, loss):
        for d in zip(reversed(self.parameters), super().forward(loss), self.G, self.Theta):
            d[2][0] = self.gamma * d[2][0] + (1 - self.gamma) * d[1][0] ** 2
            eta_w = np.sqrt(d[3][0] + self.epsilon) / np.sqrt(d[2][0] + self.epsilon)
            d[3][0] = self.gamma * d[3][0] + (1 - self.gamma) * (eta_w * d[1][0]) ** 2
            d[0].weights += eta_w * d[1][0]

            d[2][1] = self.gamma * d[2][1] + (1 - self.gamma) * d[1][1] ** 2
            eta_b = np.sqrt(d[3][1] + self.epsilon) / np.sqrt(d[2][1] + self.epsilon)
            d[3][1] = self.gamma * d[3][1] + (1 - self.gamma) * (eta_b * d[1][1]) ** 2
            d[0].bias += eta_b * d[1][1]


class Adam(Optimizer):
    def __init__(self, parameters, learning_rate, beta_1=0.9, beta_2=0.999, **kwargs):
        super().__init__(parameters)
        self.epsilon = 1e-8
        self.gamma = 0.0
        self.learning_rate = learning_rate
        self.beta_1 = beta_1
        self.beta_2 = beta_2
        self.parameters = parameters
        self.m = [[np.zeros_like(l.weights), np.zeros_like(l.bias)] for l in reversed(parameters)]
        self.v = [[np.zeros_like(l.weights), np.zeros_like(l.bias)] for l in reversed(parameters)]
        self.prev_loss = [[np.zeros_like(l.weights), np.zeros_like(l.bias)] for l in reversed(parameters)]
        self.learning_step = 1

    def step(self, loss):
        for d in zip(reversed(self.parameters), self.m, self.v, super().forward(loss)):
            d[1][0] = self.beta_1 * d[1][0] + (1 - self.beta_1) * d[3][0]
            d[2][0] = self.beta_2 * d[2][0] + (1 - self.beta_2) * d[3][0] ** 2
            aprox_m = d[1][0] / (1 - self.beta_1 ** self.learning_step)
            aprox_v = d[2][0] / (1 - self.beta_2 ** self.learning_step)
            d[0].weights += (self.learning_rate * aprox_m) / (np.sqrt(aprox_v) + self.epsilon)

            d[1][1] = self.beta_1 * d[1][1] + (1 - self.beta_1) * d[3][1]
            d[2][1] = self.beta_2 * d[2][1] + (1 - self.beta_2) * d[3][1] ** 2
            aprox_m = d[1][1] / (1 - self.beta_1 ** self.learning_step)
            aprox_v = d[2][1] / (1 - self.beta_2 ** self.learning_step)
            d[0].bias += (self.learning_rate * aprox_m) / (np.sqrt(aprox_v) + self.epsilon)
        self.learning_step += 1
