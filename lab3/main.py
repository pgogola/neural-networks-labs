from nn_layer import (
    Model,
    NNLayer,
    save_model,
    load_model,
    load_data,
    one_hot_encoder,
    mean_squared_error,
    test_accuracy,
)
import optim
import numpy as np
import time
import pickle

hidden_layer_sizes = [100,
                      200,
                      300,
                      400,
                      500,
                      600]

optimizer = [optim.SGD,
             optim.AdaGrad,
             optim.AdaDelta,
             optim.Adam]

loss_f = [optim.MSELoss, optim.AbsoluteErrorLoss]

batch_sizes = [50, 100, 150, 200, 250]
learning_rates = [0.5, 0.1, 0.05, 0.01, 0.005, 0.001]
initial_ranges = ['kaiming',
                  'xavier',
                  'uniform']
momentums = [0, 0.3, 0.6, 0.9]


def test_model(layer_sizes, batch_sizes, learning_rates, initial_ranges, momentums, optimizer_function, loss_function,
               repeats):
    test_results = []
    loss_margin = 0.05
    max_time_available = 60 * 60  # 30 minutes
    for _ in range(repeats):
        for layer_size in layer_sizes:
            for crit in loss_function:
                for opt in optimizer_function:
                    for initial_range in initial_ranges:
                        for learning_rate in learning_rates:
                            for momentum in momentums:
                                for batch_size in batch_sizes:
                                    net = Model([NNLayer(784, layer_size, "relu", initial_range),
                                                 NNLayer(layer_size, 10, "softmax", initial_range)])
                                    optimizer = opt(
                                        parameters=net.parameters(),
                                        learning_rate=learning_rate,
                                        momentum=momentum)
                                    criterion = crit()
                                    min_loss = float("inf")
                                    loss_arr = []
                                    accuracy_arr = []
                                    stop_condition = "overfitting"
                                    start_time = time.time()
                                    time_pass = 0
                                    epochs = 0
                                    while True:
                                        epochs += 1
                                        for j in range(0, len(train[1]), batch_size):
                                            result = net.forward(np.array(train[0][j:j + batch_size]))
                                            loss, d_loss = criterion(result, np.array(
                                                one_hot_encoded[j:j + batch_size]))
                                            optimizer.step(d_loss)
                                        current_loss, _ = criterion(net.forward(np.array(val[0])),
                                                                    val_one_hot_encoded)
                                        print(current_loss)
                                        if current_loss < min_loss + loss_margin:
                                            if current_loss < min_loss:
                                                min_loss = current_loss
                                                save_model(net, "./model.pkl")
                                        else:
                                            break
                                        current_accuracy = test_accuracy(net, val[0], val[1])
                                        loss_arr.append(current_loss)
                                        accuracy_arr.append(current_accuracy)
                                        time_pass = time.time() - start_time
                                        if time_pass > max_time_available:
                                            stop_condition = "time"
                                            break

                                    net = load_model("./model.pkl")
                                    final_accuracy = test_accuracy(net, test[0], test[1])
                                    test_results.append({
                                        "layer_size": layer_size,
                                        "initial_range": initial_range,
                                        "learning_rate": learning_rate,
                                        "momentum": momentum,
                                        "loss_arr": loss_arr,
                                        "accuracy_arr": accuracy_arr,
                                        "final_accuracy": final_accuracy,
                                        "stop_condition": stop_condition,
                                        "time_pass": time_pass,
                                        "epochs": epochs,
                                        "optimizer": optimizer.__class__.__name__,
                                        "loss_fun": crit.__class__.__name__
                                    })
    return test_results


if __name__ == "__main__":
    train, val, test = load_data("./resources/mnist.pkl")
    one_hot_encoded = one_hot_encoder(train[1])
    val_one_hot_encoded = one_hot_encoder(val[1])

    print("momentum")
    results = test_model(layer_sizes=[400], batch_sizes=[50],
                         learning_rates=[0.001],
                         initial_ranges=[initial_ranges[2]],
                         momentums=momentums,
                         optimizer_function=[optimizer[0]],
                         loss_function=[loss_f[0]],
                         repeats=1)
    file_name = "momentum.pkl"
    with open(file_name, 'wb') as f:
        pickle.dump(results, f, pickle.HIGHEST_PROTOCOL)

    print("adagrad")
    results = test_model(layer_sizes=[400], batch_sizes=[50],
                         learning_rates=[0.001],
                         initial_ranges=[initial_ranges[2]],
                         momentums=[1],
                         optimizer_function=[optimizer[1]],
                         loss_function=[loss_f[0]],
                         repeats=1)
    file_name = "adagrad.pkl"
    with open(file_name, 'wb') as f:
        pickle.dump(results, f, pickle.HIGHEST_PROTOCOL)

    print("adam_adadelta")
    results = test_model(layer_sizes=[400], batch_sizes=[50],
                         learning_rates=[0.001],
                         initial_ranges=[initial_ranges[2]],
                         momentums=[1],
                         optimizer_function=[optimizer[2], optimizer[3]],
                         loss_function=[loss_f[0]],
                         repeats=1)
    file_name = "adam_adadelta.pkl"
    with open(file_name, 'wb') as f:
        pickle.dump(results, f, pickle.HIGHEST_PROTOCOL)

    print("weights")
    results = test_model(layer_sizes=[400], batch_sizes=[50],
                         learning_rates=[0.001],
                         initial_ranges=initial_ranges,
                         momentums=[1],
                         optimizer_function=[optimizer[3]],
                         loss_function=[loss_f[0]],
                         repeats=1)
    file_name = "weights.pkl"
    with open(file_name, 'wb') as f:
        pickle.dump(results, f, pickle.HIGHEST_PROTOCOL)

    print("loss")
    results = test_model(layer_sizes=[400], batch_sizes=[50],
                         learning_rates=[0.0001],
                         initial_ranges=[initial_ranges[2]],
                         momentums=[1],
                         optimizer_function=[optimizer[3]],
                         loss_function=loss_f,
                         repeats=1)
    file_name = "loss.pkl"
    with open(file_name, 'wb') as f:
        pickle.dump(results, f, pickle.HIGHEST_PROTOCOL)
